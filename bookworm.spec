Name:           bookworm
Version:        1.1.1
Release:        1%{dist}
Summary:        E-book reader
License:        GPL-3.0-or-later
Group:          Applications/Publishing
URL:            https://babluboy.github.io/bookworm
Source:         https://github.com/babluboy/bookworm/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires:  ImageMagick
BuildRequires:  cmake
BuildRequires:  fdupes
BuildRequires:  gcc-c++
BuildRequires:  hicolor-icon-theme
BuildRequires:  pkgconfig
BuildRequires:  vala
BuildRequires:  pkgconfig(gee-0.8)
BuildRequires:  pkgconfig(granite) >= 0.5
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.22
BuildRequires:  pkgconfig(poppler-glib)
BuildRequires:  pkgconfig(sqlite3) >= 3.5.9
BuildRequires:  pkgconfig(webkit2gtk-4.0) >= 2.16.0
# Check list of dependencies
BuildRequires:  html2text
Requires:       html2text
Requires:       poppler-utils
Recommends:     unrar
Recommends:     unzip

%description
An eBook reader for Elementary OS.

It uses poppler for decoding and read formats like EPUB, PDF, mobi, cbr, etc.

%prep
%setup -q

chmod -x AUTHORS

%build
%cmake \
    -DGSETTINGS_COMPILE=OFF

make %{?_smp_mflags}

%install
%make_install

# fix env-script-interpreter
pushd %{buildroot}%{_datadir}
for _file in $(grep -rl '^\#\!'); do
   find -name ${_file##*/} -type f -executable -exec sed '/^\#\!/s/env\ \+//' -i {} \;
done
popd

# fix wrong-icon-size
_file=$(find -name %{name}.png)
_count=$(echo "$_file" | wc -l)
for _file in $_file; do
  ((_count -- ))
  _width=$(identify -format %w $_file)
  _height=$(identify -format %h $_file)
  _size+=$'\n'$(echo "${_width}x$_height$_file")
  [ "$_count" -eq 0 ] || continue
  _file=$(echo "$_size" | sort -rn | grep -m1 .)
  ls %{_datadir}/icons/hicolor | grep '[0-9]x[0-9]' | sort -n | while read _size; do
    if [ "${_file%x*}" -ge ${_size%x*} ]; then
      mkdir -p %{buildroot}%{_datadir}/icons/hicolor/${_size}/apps
      convert -strip ${_file#*./} -resize $_size \
        %{buildroot}%{_datadir}/icons/hicolor/${_size}/apps/${_file##*/}
    fi
  done
done

# remove executable flags
find %{buildroot} -name \*.txt -exec chmod 0644 {} +

%fdupes %{buildroot}/%{_datadir}

%files
%license LICENSE
%doc AUTHORS README.md
%{_bindir}/com.github.babluboy.bookworm
%{_datadir}/applications/com.github.babluboy.bookworm.desktop
%{_datadir}/glib-2.0/schemas/com.github.babluboy.bookworm.gschema.xml
%{_datadir}/icons/hicolor/*/apps/*bookworm*.??g
%{_datadir}/metainfo/com.github.babluboy.bookworm.appdata.xml
%{_datadir}/bookworm/
%{_datadir}/contractor/
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%changelog
* Fri Nov 09 2018 JMiahMan <jmiahman@unity-linux.org>
- rebuilt
